import os
from tornado import web , ioloop
from tornado.options import define, options
from handlers import api,views
import torndb


define("port", default=80, help="server port", type=int)
define("mysql_host", default="172.17.0.1:3306", help="omg database host")
define("mysql_database", default="omg_01", help="omg database name")
define("mysql_user", default="omg_example", help="omg database user")
define("mysql_password", default="Br0wnF0x", help="omg database password")

settings = dict(
    debug=True,
    template_path=os.path.join(os.path.dirname(__file__), "templates"),
    static_path=os.path.join(os.path.dirname(__file__), "static")
)


class Application(web.Application):

    pool = None

    def __init__(self):
        handlers = [
            (r'/', views.IndexHandler),
            (r'/api/([^/]+)', api.ApiHandler)
        ]
        super(self.__class__, self).__init__(handlers, **settings)

        # global connection to DB across all handlers
        self.pool = torndb.Connection(
            host=options.mysql_host, database=options.mysql_database,
            user=options.mysql_user, password=options.mysql_password,
            max_idle_time=25200, connect_timeout=2, charset='utf8',
            sql_mode='TRADITIONAL')


def main():
    options.parse_command_line()
    app = Application()
    app.listen(options.port)
    ioloop.IOLoop.instance().start()

if __name__ == '__main__':
    main()
