#!/usr/bin/env bash
#mysql client depend
sudo apt-get update && apt-get install -y mysql-client
#python enviorment setup
virtualenv env
#activate envlope
source ./env/bin/activate
#install requirements
pip install -r req.txt
./env/bin/deactivate