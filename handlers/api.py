from handlers.base import BaseHandler
from tornado import gen
import json

agg_roi_query = "SELECT Sub1.Website_name, Sub1.Website_url, Sub2.Page_views, Sub2.income, Sub2.spend, Sub2.profit, IFNULL(Sub2.spend/Sub2.profit, 0) as ROI FROM (SELECT Website_id, Website_name, Website_url FROM omg_01.websites) Sub1 INNER JOIN (SELECT Website_id, SUM(Page_views) AS Page_views , SUM(income) AS income, SUM(spend) AS spend, (SUM(income) - SUM(spend)) AS profit FROM omg_01.website_data GROUP BY Website_id) Sub2 ON Sub1.Website_id = Sub2.Website_id"
roi_query = "SELECT Sub1.Website_name, Sub1.Website_url, Sub2.Page_views, Sub2.income, Sub2.spend, Sub2.profit, IFNULL(Sub2.spend/Sub2.profit, 0) as ROI FROM (SELECT Website_id, Website_name, Website_url FROM omg_01.websites) Sub1 INNER JOIN (SELECT Website_id, Page_views , income, spend, (income - spend) AS profit FROM omg_01.website_data) Sub2 ON Sub1.Website_id = Sub2.Website_id"

class ApiHandler(BaseHandler):

    @gen.coroutine
    def get(self, resolution):

        if 'agg' in resolution:
            response = self.db.query(agg_roi_query)
        else:
            response = self.db.query(roi_query)

        arr = json.dumps(response)
        self.set_header("Content-Type", "application/json")
        self.write(arr)
        self.finish()





