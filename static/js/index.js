$(document).ready(function() {
  var ajax_url = "api/norm"
  var ajax_agg_url = "api/agg"

  table = $('#datatable').DataTable({
    "ajax":{
      "url": ajax_url,
      "dataSrc": ""
    } ,
    rowCallback: function(row, data, index){
    $('td:eq(0)', row).html('<a href="'+data['Website_url']+'">'+data['Website_name']+'</a>' );
  	if(data['ROI']> 0){
    	$(row).find('td:eq(5)').css('color', 'green');
    }
    else{
    $(row).find('td:eq(5)').css('color', 'red')
    }},
    "columns": [
      { "data": "Website_name" },
      { "data": "Page_views" },
      { "data": "income" },
      { "data": "spend" },
      { "data": "profit" },
      { "data": "ROI" }]
  });

} );