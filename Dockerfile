FROM python:2.7

EXPOSE 80

RUN apt-get update && apt-get install -y mysql-client
# based on python:2.7-onbuild, but if we use that image directly
# the above apt-get line runs too late.
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY req.txt /usr/src/app/
RUN pip install -r req.txt

COPY . /usr/src/app

CMD python server.py --port=80 --mysql_host=mysql