CREATE DATABASE omg_01

CREATE TABLE omg_01.websites
(
Website_id int,
Website_name varchar(255),
Website_url VARCHAR(2083)
);

CREATE TABLE omg_01.website_data
(
Website_id int,
Page_views double,
income float,
spend float
);

INSERT INTO omg_01.websites
    (Website_id, Website_name, Website_url)
VALUES
    (1, "Loan pride", "http://loanpride.com/"),
    (2, "Proud Democrat", "http://proudemocrat.com/"),
    (3, "Rehab Junkys", "http://rehabjunkys.com"),
    (4, "Game of Glam", "http://gameofglam.com/"),
    (5, "Rehab Center near me", "http://rehabcenternearme.com/")

INSERT INTO omg_01.website_data
    (Website_id, Page_views, income, spend)
VALUES
    (1, 1524, 150, 145),
    (1, 122256, 320.5, 220.2),
    (2, 123432, 70.5, 77.2),
    (2, 34325, 97.3, 96.5),
    (3, 98456, 66.5, 65.5),
    (3, 775423, 102.5, 103.5),
    (4, 756432, 240.5, 210.4),
    (4, 332435, 324.2, 289.4),
    (4, 16543, 120.5, 98.1),
    (5, 45438, 254.6, 215.1),
    (5, 1854782, 659.8, 524.2)
